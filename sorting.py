import glob
import os
from shutil import copyfile

emotions = ["neutral", "anger", "contempt", "disgust", "fear", "happy", "sadness", "surprise"]  # Define emotion order
sets = sorted(glob.glob("emotion/*"))  # Returns a list of all folders with participant numbers


for x in sets:
    setnumber = "%s" % x[-4:]  # store current set number

    for sessions in sorted(glob.glob("%s/*" % x)):  # Store list of sessions for current participant
        for files in sorted(glob.glob("%s/*" % sessions)):
            current_session = files[13:-30]
            file = open(files, 'r')

            emotion = int(
                float(file.readline()))  # emotions are encoded as a float, readlias float, then convert to integer.
            sourcefile_emotion = sorted(glob.glob("images/%s/%s/*" % (setnumber, current_session)))[
                -1]  # get path for last image in sequence, which contains the emotion

            sourcefile_neutral = sorted(glob.glob("images/%s/%s/*" % (setnumber, current_session)))[
                0]  # do same for neutral image
            dest_neut = "sorted/neutral/%s" % sourcefile_neutral[16:]  # Generate path to put neutral image

            dest_emot = "sorted/%s/%s" % (
            emotions[emotion], sourcefile_emotion[16:])  # Do same for emotion containing image
            copyfile(sourcefile_neutral, dest_neut)  # Copy file
            copyfile(sourcefile_emotion, dest_emot)  # Copy file